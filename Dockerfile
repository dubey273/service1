FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENV SHELL /bin/bash
ENTRYPOINT ["java","-jar","/app.jar"]
CMD ["bash"]